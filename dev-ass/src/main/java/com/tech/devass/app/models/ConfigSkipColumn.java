package com.tech.devass.app.models;

public class ConfigSkipColumn {
	private String oper, partOfColumnName;

	public ConfigSkipColumn(String oper, String partOfColumnName) {
		this.oper = oper;
		this.partOfColumnName = partOfColumnName;
	}

	public String getOper() {
		return oper;
	}

	public String getPartOfColumnName() {
		return partOfColumnName;
	}

}
