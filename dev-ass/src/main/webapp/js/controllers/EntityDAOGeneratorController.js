angular.module('MetronicApp').controller('EntityAndDAOGeneratorController', function($rootScope, $scope, $http, $timeout) {
	var vm = this;
	$scope.$on('$viewContentLoaded', function() {   
        // initialize core components
        App.initAjax();
        
        var grid = new Datatable();

        grid.init({
            src: $("#datatable_ajax"),
            onSuccess: function (grid) {
                // execute some code after table records loaded
            },
            onError: function (grid) {
                // execute some code on network or other general error  
            },
            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options 

                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js). 
                // So when dropdowns used the scrollable div should be removed. 
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
            	
                "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.

                "lengthMenu": [
                    [10, 20, 50, 100, 150, -1],
                    [10, 20, 50, 100, 150, "All"] // change per page values here
                ],
                "pageLength": 10, // default record count per page
                "ajax": {
                    "url": "bootstrap/modelgen/summaryTables", // ajax source
                },
                "order": [
                    [1, "asc"]
                ], // set first column as a default sort by asc
                "columns": [
                    {"data":"checkbox"},
                    {"data":"name"},
                    {"data":"keys"},
                    {"data":"status"}
                ],
                "processing":true
            }
        });
        
        var grid = $('#datatable_ajax').DataTable();
	    $("#datatable_ajax").on('click', 'tr', function(){
	    	$("#entityCodeTxt").val(grid.row( this ).data().entityCode);
	    	$("#daoCodeTxt").val(grid.row( this ).data().daoCode);
	    }); 
        
        /**
         * 
         * CODE BELOW NOT WORK
         * $scope in $viewContentLoaded not same $scope out side
         */
//        $("#datatable_ajax").on('click', 'tr', $scope.ok);
        
//        $("#datatable_ajax").on('click', 'tr', function(){
//        	console.log($scope.code.entity);
//        	console.log(grid.row( this ).data());
//        	console.log(grid.row( this ).data()["entityCode"]);
//        	console.log(grid.row( this ).data().entityCode);
//        	$scope.code.entity = 'aa';
//        	$scope.$apply();
//        	$scope.code.dao = eval(grid.row( this ).data()).daoCode;
//        	$rootScope.$apply();
//        	console.log($scope.code.entity);
//        }); 
    });

    // set sidebar closed and body solid layout mode
    $rootScope.settings.layout.pageContentWhite = true;
    $rootScope.settings.layout.pageBodySolid = false;
    $rootScope.settings.layout.pageSidebarClosed = false;
    
    $scope.tables = [];
    
    $scope.showTableClick = function(){
    	alert("NOT IMPLEMENT YET!");
    };
    
    $scope.connection = {
    	driver:'',
    	url:'',
    	username:'',
    	password:'',
    };
    
    $scope.code = {
    	entity:'',
    	dao:''
    };
    
    $scope.copy = function(element){
    	$(element).select();
		try {
		  var successful = document.execCommand('copy');
		  var msg = successful ? 'successful' : 'unsuccessful';
		  console.log('Copying text command was ' + msg);
		} catch (err) {
		  console.log('Oops, unable to copy');
		}
    };
    
});