package com.tech.bootstrap.models;

public class BootStrapOrderRequestModel {
	public int column;
	public String dir;
	@Override
	public String toString() {
		return "BootStrapOrderRequestModel [column=" + column + ", dir=" + dir
				+ "]";
	}
	
}
