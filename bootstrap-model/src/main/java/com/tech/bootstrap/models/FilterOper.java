package com.tech.bootstrap.models;


public interface FilterOper<T> {
	public boolean filter(T t, BootStrapTableRequestModel model);
}
