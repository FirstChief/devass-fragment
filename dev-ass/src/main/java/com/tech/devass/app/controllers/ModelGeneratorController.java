package com.tech.devass.app.controllers;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tech.devass.app.services.ModelGenerator;

@Controller
@RequestMapping("/modelgen")
public class ModelGeneratorController {
	
	@Autowired
	ModelGenerator modelGenerator;

	@RequestMapping(value = "/test", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> test(
			@RequestParam String test) {
		Map<String,Object> ret = new HashMap<>();
		ret.put("result", "ok");
		return ret;
	}
	
	@RequestMapping(value = "/summaryTables", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> summaryTables() throws Exception {
		Map<String,Object> ret = new HashMap<>();
		ret.put("tables", modelGenerator.summaryTables());
		return ret;
	}
}
