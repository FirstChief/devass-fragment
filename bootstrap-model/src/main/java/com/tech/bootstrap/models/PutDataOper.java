package com.tech.bootstrap.models;

import java.util.Map;

public interface PutDataOper<T> {
	public void put(Map<String,Object> data, T t);
}
