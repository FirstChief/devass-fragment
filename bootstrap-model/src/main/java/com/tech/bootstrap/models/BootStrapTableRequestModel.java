package com.tech.bootstrap.models;

import java.util.HashMap;
import java.util.Map;

public class BootStrapTableRequestModel {

	public int draw;
	public BootStrapColumnRequestModel[] columns;
	public BootStrapOrderRequestModel[] order;
	public int start;
	public int length;
	public Map<String, String> search;
	
	public static BootStrapTableRequestModel parse(Map<String,String[]> reqModel) {
		BootStrapTableRequestModel model = new BootStrapTableRequestModel();
		model.draw = Integer.parseInt(reqModel.get("draw")[0]);
		model.start = Integer.parseInt(reqModel.get("start")[0]);
		model.length = Integer.parseInt(reqModel.get("length")[0]);
		
		Map<String, String> search = new HashMap<>();
		for(String key:reqModel.keySet()){
			if(!key.startsWith("filter_")){
				continue;
			}
			search.put(key.replace("filter_", ""), reqModel.get(key)[0].toString());
		}
		model.search = search;
		
		int colCount = 0;
		for(int i=0;i<100;i++){
			if(reqModel.get("columns["+i+"][data]") != null){
				continue;
			}
			colCount = i;
			break;
		}
		BootStrapColumnRequestModel[]columns = new BootStrapColumnRequestModel[colCount];
		for(int i=0;i<colCount;i++){
			columns[i] = new BootStrapColumnRequestModel();
			columns[i].data = reqModel.get("columns["+i+"][data]")[0];
		}
		model.columns = columns;
		
		int ordCount = 0;
		for(int i=0;i<100;i++){
			if(reqModel.get("order["+i+"][column]") != null){
				continue;
			}
			ordCount = i;
			break;
		}
		BootStrapOrderRequestModel[]ords = new BootStrapOrderRequestModel[ordCount];
		for(int i=0;i<ordCount;i++){
			ords[i] = new BootStrapOrderRequestModel();
			ords[i].column = Integer.parseInt(reqModel.get("order["+i+"][column]")[0]);
			ords[i].dir = reqModel.get("order["+i+"][dir]")[0];
		}
		model.order = ords;
		
		return model;
	}
}
