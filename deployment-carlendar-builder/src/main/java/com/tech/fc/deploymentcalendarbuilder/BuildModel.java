package com.tech.fc.deploymentcalendarbuilder;

import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import org.apache.poi.xssf.usermodel.XSSFClientAnchor;
import org.apache.poi.xssf.usermodel.XSSFDrawing;
import org.apache.poi.xssf.usermodel.XSSFShape;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFSimpleShape;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.w3c.dom.Node;

public class BuildModel {

	public class DayBox implements Comparable<DayBox> {
		XSSFSimpleShape shape;
		Node node;
		String nodeVal;
		int nodeSeq;

		@Override
		public int compareTo(DayBox o) {
			return this.nodeSeq - o.nodeSeq;
		}
	}

	private List<DayBox> collectDayBox(XSSFDrawing pat) {
		List<DayBox> ret = new ArrayList<>();
		List<XSSFShape> shapes = pat.getShapes();
		int no = 0;
		for (XSSFShape shape : shapes) {
			XSSFSimpleShape shape2 = (XSSFSimpleShape) shape;
			// System.out.println((++no) + ":" + shape.getClass());
			// System.out.println(shape2.getCTShape().getTxBody()
			// .getPList().get(0));
			XSSFClientAnchor archor = (XSSFClientAnchor) shape2.getAnchor();
			// System.out.println(archor.getFrom()+","+archor.getTo());
			if (shape2.getCTShape().getTxBody().getPList().get(0).getDomNode()
					.getLastChild().getLastChild() == null) {
				continue;
			}
			String nodeVal = shape2.getCTShape().getTxBody().getPList().get(0)
					.getDomNode().getLastChild().getLastChild().getLastChild()
					.getNodeValue();

			Node lastChild = shape2.getCTShape().getTxBody().getPList().get(0)
					.getDomNode().getLastChild().getLastChild().getLastChild();

			if (nodeVal.startsWith("d")) {

				String noStr = nodeVal.substring(1);
				int nodeSeq = Integer.parseInt(noStr);

				DayBox dayBox = new DayBox();
				dayBox.node = lastChild;
				dayBox.nodeSeq = nodeSeq;
				dayBox.shape = shape2;

				ret.add(dayBox);
			}
			// System.out.println(shape2.getCTShape().getTxBody()
			// .getPList().get(0).getDomNode().getLastChild()
			// .getLastChild().getNodeName());
			// System.out.println(nodeVal);
		}
		return ret;
	}

	public void build(int forYear, XSSFWorkbook template) {
		try {
			
			SimpleDateFormat monthFmt = new SimpleDateFormat("MMM",
					Locale.US);
			
			int sheetCount = template.getNumberOfSheets();
			// System.out.println("sheetCount:" + sheetCount);
			for (int i = 0; i < 12; i++) {
				XSSFSheet sheet1 = template.getSheetAt(i);

				XSSFDrawing pat = sheet1.createDrawingPatriarch();

				List<DayBox> dayBoxs = collectDayBox(pat);
				Collections.sort(dayBoxs);

				Calendar calendar = GregorianCalendar.getInstance(Locale.US);
				calendar.set(Calendar.YEAR, forYear);
				calendar.set(Calendar.MONTH, i);
				calendar.set(Calendar.DAY_OF_MONTH, 1);
				
				sheet1.getRow(0).getCell(1).setCellValue(
						"" + monthFmt.format(calendar.getTime()));
				sheet1.getRow(0).getCell(2).setCellValue(
						"" + forYear);
				template.setSheetName(i, 
						monthFmt.format(calendar.getTime()) + " " 
						+ forYear);
				
				int firstDayOfWeekOfMonth = calendar.get(Calendar.DAY_OF_WEEK);
//				System.out.println(firstDayOfWeekOfMonth);
				int maximumDayOfMonth = calendar
						.getMaximum(Calendar.DAY_OF_MONTH);

//				System.out.println(calendar.getTime());
//				System.out.println(maximumDayOfMonth);

				int ii = 1;
				int dayOfMonth = 1;
				for (DayBox dayBox : dayBoxs) {
					if (ii < firstDayOfWeekOfMonth || dayOfMonth > maximumDayOfMonth) {
						dayBox.node.setNodeValue("");
					} else {
						dayBox.node.setNodeValue(dayOfMonth++ + "");
					}
					ii++;
				}
			}

			template.write(new FileOutputStream(
					"C:\\Users\\admin\\Desktop\\DeploymentCalendar" + forYear
							+ ".xlsx"));
		} catch (Exception ex) {
			ex.printStackTrace(System.err);
		}
	}
}
