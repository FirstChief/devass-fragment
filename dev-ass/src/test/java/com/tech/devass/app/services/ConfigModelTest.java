package com.tech.devass.app.services;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.tech.devass.app.models.ConfigModel;
import com.tech.devass.app.models.ConfigSkipType;
import com.tech.devass.app.models.DBTabInfo;
import com.tech.devass.app.models.JavaTypeDef;

public class ConfigModelTest {

	@Test
	public void test(){
		assertTrue(true);
	}

	@Test
	public void testParseConfig() throws Exception{
		ConfigModel model = ConfigModel.parse("D:\\techsphere\\dev\\configuration.xlsx");
		List<ConfigSkipType> skipTypes = model.getSkipTypes();
		for(ConfigSkipType st:skipTypes){
			System.out.println(st.getOper()+":"+st.getPartOfTypeName());
		}
	}
}
